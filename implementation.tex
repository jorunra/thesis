% \input{introduction.tex}
% \input{background.tex}
% \input{xeonPhi.tex}
% \input{preparation.tex}
% \input{methods.tex}
% \input{result.tex}
% \input{future.tex}
% \input{implementation.tex}

The implementation process of this thesis' alignment tool featured a handful of
steps. Firstly to make a prototype to get to know the flow of an alignment
process and the algorithm involved, the transition to an implementation
utilizing the unique Xeon Phi architecture, and lastly the optimization to gain
the best possible performance.


\section{Prototype}
The first implementing task was to make a simple prototype with the goal to
locate and attain the optimal local alignment without optimization and workload
distribution. From the dive into the pros and cons for both BLAST and
some Smith-Waterman approaches, the decision to use dynamic programming for the
Xeon Phi implementation came naturally. The prototype was a way to get to
know the algorithm and the challenges associated with it.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/prototype.png}}
\caption[The prototype]{The prototypes main steps \label{proto}}
\end{figure}

The main functionality the prototype needed was the steps shown in Figure
\ref{proto}. A way of retrieving both query and database sequences, a function
utilizing the Smith-Waterman algorithm to align the sequences and pass on the
scoring value, and of course a comparison to ensure the sequence with the
highest score computed got saved.

For sequence retrieval a file in FASTA format was read line by line and stored in
a sequence struct containing the sequence of amino acids and the length of the
sequence. The sequences description is redundant while aligning a sequence,
therefore it is skipped and never saved to memory.

To align the sequences, the Smith-Waterman algorithm together with Gotoh's
\cite{gotoh_improved_1982} modification, is used. The calculation is carried out
column wise as shown in Figure \ref{fig:columns}. Only the previously calculated
row of the H and E array is necessary in the calculation, and therefore the only
space allocated in memory. The highest score achieved for any element of H is
stored and returned as the alignment score.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/columns.png}}
\caption[Column-wise calculation]{The alignment is carried out column-wise.
\label{fig:columns}}
\end{figure}

As an extension, an additional function doing the same as the earlier mentioned
Smith-Waterman function is called for the sequence with the best score to
compose the full H array to locate the local alignment within the sequence. This
H array is sent to a function that locates the highest score, backtrace
until a 0 is found and storing a three string representation containing the
query sequence, a symbol representation of the alignment and the database
sequence, all with respect to insertions and deletions.

Due to all of the existing sequence alignment tools the credibility of the
prototype was easy to determine. If the prototype yielded the same optimal
sequence(s) as a leading tool, like SWIPE, the prototype is likely to be
correct. The further development was then to transfer the prototype to execute
on the Xeon Phi. This included workload distribution and memory management.

\section{SWIMIC}
With the prototype finish, a tool actually designed for the Xeon Phi coprocessor
needed to be implemented. The implementation was to run on the Many Integrated
Core (MIC) architecture utilizing the Smith-Waterman algorithm, therefor a
suitable name for the finish tool was SWIMIC. Short for \textit{the
    Smith-Waterman algorithm Implemented for MIC architecture}.


\section{Framework and Execution Model}
Before the Xeon Phi implementation began some decisions concerning framework for
parallelization and an execution model for the Xeon Phi had to be determined.
% TODO Short into

\subsection*{MPI}
Initially the thought was to use MPI (Message Passing Interface) as the
framework for parallelization.  MPI is what Rognes' tool SWIPE
\cite{rognes_faster_2011} uses together with SIMD vectorization, and it seemed
reasonable to use the same approach for this thesis work. The thought of using
something else never appeared until executing a simple MPI implementation of the
prototype on the Xeon Phi and the run time was slower than on a Xeon
processor.

How could it be that a parallel implementation executed slower on an
architecture that was made for parallelism? The explanation was simple and
should have been spotted at a much earlier stage. The Xeon Phi has a shared
memory pool, and MPI is constructed for communication between processes on a
distributed memory system. The MPI prototype was executing poorer because it was
unnecessary distributing memory that the kernels on the Xeon Phi already had
access to.

\subsection*{OpenMP} \label{sec:omp}
The other parallelization framework available for the Xeon Phis is the OpenMP
API. OpenMP is used to distribute the workload between the cores.  If the host
processor is included in the calculation, both OpenMP and MPI may be used for
communication between the host and the coprocessor. For all other purposes
OpenMP is the obvious choice.

OpenMP is an implementation of multi-threading where a master thread is
executing the program and forks out a specified number of slave threads when a
pre-specified parallel part is reached. It follows the general idea of parallel
execution shown in Figure \ref{parallel} on page \pageref{parallel}. A section
that is meant to run in parallel is marked accordingly by pragmas directives.
For instance \textit{\#pragma omp parallel for schedule(dynamic)
num\_threads(THREADS)} runs the following for loop in parallel with THREADS
number of threads and the workload is divided among them dynamically during
runtime. After the execution of the parallel section, the threads join back into
the master thread, which continues onward to the end of the program.

For OpenMP to be utilized to the best of its ability the size of the task to be
done in parallel must be known before it starts the execution. For instance how
many iteration in a loop before it is executed, thus for loops are preferred
over while loops.

\subsection*{Execution Model}
In Section \ref{sec:ex-mod} some pros and cons concerning either a native or
offload execution model where presented. Since a sequence alignment tool more or
less only perform the Smith-Waterman algorithm \cite{smith_identification_1981}
which falls under the category of mapping to parallelism either in threads or
vectors, the native execution model was chosen. The relatively small usage of
I/O work, as well as a command line only application also favored the native
execution model.

\section{Preparation of the Database}
To make sure the alignment process had the best starting point as possible, a
preprocessing of the database was necessary.

\subsection{Sorting}
The preparation contained gaining the knowledge of the size in sequences and
characters, and a sorting of the sequences before writing them back to file
staring with the longest to shortest. The sorting was done to even out the
workload distributed to each thread, preventing one thread from receiving all
the long sequences and finishing much later than the others. The sorting only
works if the workload is distributed in small chunks with the intention that all
threads calculate a number of chunks and get a new chunk when they finish their
task, thus the shorter, hence smaller tasks are given out at the end.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/binaryTree.png}}
\caption{The binary tree structure used for sorting the database \label{binary}}
\end{figure}

For the sorting of the database a binary tree structure, shown in figure
\ref{binary}, was used. There was no need for a balanced tree since it is never
used for searching, its only purpose was to keep the sequence order and write
them back to file in a recursive function starting form right to left, i.e.
longest to shortest sequence.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.7]{figures/pdb.png}}
\caption{The processed database file \label{pdb}}
\end{figure}

Figure \ref{pdb} shows the processed database file with the total amount of
sequences represented by a \texttt{long} as the first element, the total length
of the database represented in \texttt{char}'s as the second element, and
continuing through the entire database with a \texttt{long} that tells the
length of the next sequence followed by the corresponding fixed length
description.

The characters are mapped to a corresponding number between 0-27 before written
to file. This to make the look ups in the substitution scoring matrix faster
since there is no need to see what character it is, the score is now found by
using the \texttt{int} value of the database entry for the \texttt{j} index and
the \texttt{int} value of the query entry for the \texttt{i} index in the
scoring matrix. The mapping is as Figure \ref{map} shows.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/map.png}}

% \begin{framed}
% \tiny{
% \begin{adjustwidth}{0.8 cm}{-1 cm}
% \begin{verbatim}
% 
% 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
% -  A  B  C  D  E  F  G  H  I  K  L  M  N  P  Q  R  S  T  V  W  X  Y  Z  U  *  O  J
% \end{verbatim}
% \end{adjustwidth}
% }
% \end{framed}
\caption[Character mapping]{\label{map} The mapping from characters to corresponding values}
\end{figure}

The preprocessed database is rapidly read into memory without reallocation and
end of file (\texttt{EOF}) check since the required size is loaded first.

\subsection{Distribution} \label{sec:dist}
To gain an optimal performance on the Xeon Phi, Section \ref{sec:gop} presents
some criteria that is useful to follow. Among them, the importance of conscious
memory usage. To fulfill this requirement a distribution of the database was
necessary to prevent each thread from extracting data form \textit{16} different
locations in memory when building a 512 bit vector of 16 32 bit \texttt{int}
values. Without a change in the database this poor utilization of cache would
have caused a huge performance bottleneck as a result of unnecessary memory
reads.

The solution was to pair a given number \textit{N} of sequences and write them
to file with all the 16 first characters first, then the second character for
all sequences and so on. When the end is reached for a sequence it is padded
with \textit{'*'} until the length of the longest of the \textit{N} sequences is
reached to ensure that all \textit{N} sequences is the same length and can be
calculated in the same vector. Shown in Example \ref{exp:2}

\begin{figure}[!h]
\begin{framed}
\small
\begin{adjustwidth}{0.4 cm}{0.4 cm}
\begin{exmp} \label{exp:2}
An example with a simplified database with the following four same letter sequences.

\begin{center}
\begin{tabular}{cccccccccccc}
F&F&F&F&F&F&F&F&F&F&F&F \\
M&M&M&M&M&M&M&M&M&M&M \\
D&D&D&D&D&D&D&D&D&D \\
Y&Y&Y&Y&Y&Y \\
\end{tabular}
\end{center}

\noindent
First they are padded to be the same length.

\begin{center}
\begin{tabular}{cccccccccccc}
F&F&F&F&F&F&F&F&F&F&F&F \\
M&M&M&M&M&M&M&M&M&M&M&* \\
D&D&D&D&D&D&D&D&D&D&*&* \\
Y&Y&Y&Y&Y&Y&*&*&*&*&*&* \\
\end{tabular}
\end{center}

\noindent
Then they are merged into one combined sequence with the first letter in all
sequences, then the second letter and so on.

\begin{center}
\begin{tabular}{cccccccccccc}
F& M& D& Y& F& M& D& Y& F& M& D& Y \\
F& M& D& Y& F& M& D& Y& F& M& D& Y \\
F& M& D& *& F& M& D& *& F& M& D& * \\
F& M& D& *& F& M& *& *& F& *& *& * \\
\end{tabular}
\end{center}

\noindent
The padding is also included to
make it possible to extract the initial sequences by assembling every fourth
letter. \\
\end{exmp}
\end{adjustwidth}
\end{framed}
\end{figure}


After \textit{N} sequences and \textit{N} descriptions are written, a new group
starts. The length of the longest sequence in that group first, then the
\textit{N} next sequences padded and distributed and the corresponding
descriptions. A drawing of the distributed database is shown in Figure
\ref{ddb}.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.65]{figures/distributed_db.png}}
\caption{The distributed database file \label{ddb}}
\end{figure}

The reason for two entries with the length of the database is because of the
padding to make the \textit{N} grouped sequences the same length,
\texttt{residues} gives the correct number of residues in the database while the
\texttt{len\_total} includes the padded chars to give the right length for
memory allocation. With this modification the cached data is the next
requested data by the application and the cache is utilized in an efficient and
conscious matter.

\section{Memory Management}
To attain the conscious cache utilization prepared in the preprocessed database
the loaded sequences are stored in a continuous \texttt{char} array like Figure
\ref{fig:db-mem-dist} shows.  The database array is allocated using the
\textit{memory aligned} \texttt{\_mm\_malloc} function to assure the required 64
byte memory alignment.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.7]{figures/db-mem-dist.png}}
\caption{Database in memory \label{fig:db-mem-dist}}
\end{figure}

The remaining details about each sequence is stored in an array of the custom
struct \texttt{idx\_t}. Each sequence has its own entry with all required
information such as the description and space to hold the later calculated
score. The \texttt{idx\_t} entry is also used to navigate the database array
using the \texttt{index} variable stored in the entry. Figure \ref{fig:idx}
presents this structure with corresponding colors to Figure
\ref{fig:db-mem-dist}.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.7]{figures/idx_t.png}}
\caption{Table of contents for the database in memory \label{fig:idx}}
\end{figure}

Since all database sequences has its own entry, there is no thread interfering
while calculating the optimal alignment with the Smith-Waterman algorithm, hence
no need for critical sections while executing and storing the alignment. This,
in addition to memory aligned allocations, addresses well with the memory
criteria for gaining optimal performance in Section \ref{sec:gop} on page
\pageref{sec:gop}.

\subsection{Scoring Matrices}
The scoring matrices are an important component in the alignment process. Every
cell calculation in the H matrix requires a lookup in the scoring matrix, thus
the importance of time and cache efficient retrieval of alignment scores. Due to
the fact that the values in a scoring matrix never exceeds the highest or
lowest value possible to represent with 8 bits, the scoring matrices are
represented by a 64 byte aligned \texttt{int8\_t} array.

Since the database and query sequences are mapped close to alphabetically to
values as shown in Figure \ref{map} on page \pageref{map}, a alphabetically
sorted scoring matrix makes for efficient lookups. The blosum62 matrix is sorted
and illustrated in Figure \ref{fig:ssm}. The included scoring matrices are
\texttt{blosum45}, \texttt{blosum50}, \texttt{blosum62}, \texttt{blosum80} and
\texttt{blosum90} in the blosum series. From the pam series \texttt{pam30},
\texttt{pam70} and \texttt{pam250} is included.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.75]{figures/sorted-blosum62.png}}
\caption[Scoring matrix sorted]{\label{fig:ssm} The scoring matrix blosum62
sorted alphabetically and mapped to corresponding values}
\end{figure}

\section{Threading}
The most efficient way to utilize OpenMP is to set up the work to be done in a
for loop and let the OpenMP API distribute the workload, either statically by
giving a fixed chunk to each thread or dynamically where the distribution is
done at runtime. For SWIMIC the most promising way to set up the workload was to
use a for loop iterating through the sequences and let OpenMP distribute the
inner calculations dynamically, hence the sorting of the database sequences from
longest to shortest to avoid a large workload being distributed to one thread
at the end of the calculation and when all other threads are finish they have to
wait for the last one. With this workload distribution the scalability to more
than a hundred threads is good and the scalability criteria from Section
\ref{sec:gop} is fulfilled.


\subsection{Affinity} \label{s_aff}
OpenMP has some built-in features on how to distribute the workload between
cores; \textit{compact}, \textit{scatter}, \textit{balanced} and \textit{none}.
Since there is 4 hardware threads per core it may make a difference in what
order the tasks are divided among the cores. As Figure \ref{omp_affinity}
shows, \textit{scatter}, \textit{compact} and \textit{balanced} have a
predefined pattern on how to distribute, while \textit{none} just randomly
distributes the tasks. In this simple example only 8 of 16 threads where used,
but the same principles applies when all threads are contributing. If the tasks
distributed are independent on each other, a \textit{scatter} or
\textit{balanced} affinity is more likely to be efficient. On the other hand, if
the tasks use a lot of the same data, a \textit{compact} affinity may yield a
better result, since closely cached data is more quickly retrievable.

\begin{figure}[H]
\centerline{\includegraphics[scale = 0.7]{figures/omp_affinity.png}}
\caption{OpenMP affinity distribution}{\label{omp_affinity}}
\end{figure}

The thread affinity interface of the Intel runtime library can be controlled by
using the \texttt{KMP\_AFFINITY} environment variable.


\section{Vectorization}
To fully utilize the Xeon Phi architecture, an effective and well thought out
implementation is needed. The most unique feature is the 512 bit vector unit
which provides an exceptional opportunity to perform the same action to a large
multiple of sequences at once. The desirable outcome of vectorization on the
Xeon Phi has to be able to triple the number of actions performed simultaneously
compared to a regular CPU with a 128 bit vector unit, hence a close to triple
speed gain. Swipe \cite{rognes_faster_2011} uses SIMD intrinsics to perform 16
smith-waterman search simultaneously by using only 8 bit of the 128 bit vector
unit available in a regular CPU per database sequence. Because of the small
space given to each sequence, overflow/underflow is unfortunately a problem and
needs to be accounted for.

When looking through the list of the Xeon Phi's intrinsics it was like a bucket
of cold water was thrown in this thesis direction. The Xeon Phi did not support
operations on smaller data types than \texttt{int}, e.i. 32 bit! That meant the
end for the anticipated threefold increase in actions and speed, since dividing
the 512 bit vector unit into 32 bit only adds up to 16, which is the same
multiple of sequences as Swipe.

On a slight positive, the need for overflow/underflow check when utilizing
calculations on 8 bit disappeared, since the score for an alignment would never
exceed the \texttt{INT\_MAX} value of 32767.  However, this is nothing compared
to the performance gain lost by the unsupported smaller data types.

The algorithm to vectorize is the Smith-Waterman algorithm
\cite{smith_identification_1981} with Gotoh's \cite{gotoh_improved_1982}
modification discussed in section \ref{sw_al}. The algorithm is included below
to refresh the memory and to look at when the steps in the algorithm is
vectorized.\\

\noindent
There are four main steps in the algorithm:

\begin{itemize}
    \item Find the score from the scoring matrix corresponding to the query and
          the database sequence being aligned, and add the score to H.
    \item Find max of H, E and F, and make sure it is not a negative number.
    \item Save score in S if the score is higher than previously calculated max
    \item Update H, E, and F to reflect potential gap openings and/or extensions.
\end{itemize}

\begin{align*}
H_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i-1,j-1} + SM[q_i,d_j] $\\
            $E_{i,j}$\\
            $F_{i,j}$\\
            $0$\\
        \end{tabular}
    \right\rvert
    \begin{tabular}{c}
        $i > 0$\\
        $\cap$\\
        $j > 0$\\
    \end{tabular}$\\
    $\qquad \qquad \quad  0 \qquad\qquad\qquad  $ $\quad \left\lvert
    \begin{tabular}{c}
        $i = 0$\\
        $\cup$\\
        $j = 0$\\
    \end{tabular} \right .$
\end{tabular}
\right .\\\\
E_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i,j-1} - Q $ \\
            $E_{i,j-1} - R $
        \end{tabular}
    \right\rvert j > 0 $ \\
    $\qquad \quad 0 \qquad $  $ \quad $ $\quad \rvert j = 0$
\end{tabular}
\right . \\\\
F_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i-1,j} - Q $ \\
            $F_{i-1,j} - R $
        \end{tabular}
        \right\rvert i > 0 $ \\
        $\qquad \quad 0 \qquad $  $ \quad $ $\quad \rvert i = 0$
\end{tabular}
\right .\\\\
S &= \quad \underset{1 \leq i \leq m \cap 1 \leq j \leq n }{\text{max }} \quad
H_{i,j}\\
\end{align*}

Since E and F holds the value for the previous step, the calculation of gaps can
be performed as the last step for the current entry to exploit the cache. A
simplified SIMD vectorization is shown in Figure \ref{code} using vectorized add
and subtraction along with the ability to find the maximum value of each element
in two vectors, all simple instructions the coprocessor possesses.

The reason why a for loop is used for setting up the score matrix vector (SM)
instead of the \texttt{\_mm512\_set\_epi32} intrinsic is due the fact that auto
vectorization of loops on the Xeon Phi proves to be faster then the \texttt{set}
function.


\begin{figure}[!h]
\begin{framed}
\footnotesize{
%\begin{adjustwidth}{0.8 cm}{-1 cm}
\begin{verbatim}
// -------- step 1 ------------

for (t = 0; t < SW_MULTIPLE; t++) {
    tmp[t] = score_matrix(db, query);
}
SM = *(__m512i*)tmp;
H = _mm512_add_epi32(H,SM);

// -------- step 2 ------------

H = _mm512_max_epi32(H,E);
H = _mm512_max_epi32(H,F);
H = _mm512_max_epi32(H,zero);

// -------- step 3 ------------

S = _mm512_max_epi32(H,S);

// -------- step 4 ------------

E = _mm512_sub_epi32(E,GAPEXTEND);
F = _mm512_sub_epi32(F,GAPEXTEND);
H = _mm512_sub_epi32(H,GAPOPENEXTEND);
E = _mm512_max_epi32(H,E);
F = _mm512_max_epi32(H,F);

\end{verbatim}
%\end{adjustwidth}
}
\end{framed}
\caption[SIMD code]{\label{code} SIMD code}
\end{figure}

For an easier and more flexible way of doing the alignment a define was created,
shown in Figure \ref{code_align}


\begin{figure}[!h]
\begin{framed}
\footnotesize{
%\begin{adjustwidth}{-0.8 cm}{-1 cm}
\begin{verbatim}

#define ALIGN(H, N, E, F, SM, S)                               \
H = _mmx_add_epi32(H,SM);    /* add comparability score to H */\
H = _mmx_max_epi32(H,F);     /* MAX (H, F)                   */\
H = _mmx_max_epi32(H,zero);  /* Make sure H > 0              */\
H = _mmx_max_epi32(H,E);     /* MAX (H, E)                   */\
S = _mmx_max_epi32(H,S);     /* Save max score               */\
N = H;                       /* Save H for next step         */\
H = _mmx_sub_epi32(H,GOE);   /* SUB gap open-extend          */\
F = _mmx_sub_epi32(F,GE);    /* SUB gap extend               */\
F = _mmx_max_epi32(H,F);     /* Test for opening or extend   */\
E = _mmx_sub_epi32(E,GE);    /* SUB gap extend               */\
E = _mmx_max_epi32(H,E);     /* Teat for opening or extend   */

\end{verbatim}
%\end{adjustwidth}
}
\end{framed}
\caption{SIMD ALIGN code\label{code_align}}
\end{figure}




% TODO New section or incorporate with the previous one. This applies to the
% three next subsection
\subsection{Additional Alignments} \label{sec:adal}
By increasing the number of database sequences aligned per character in the query,
the utilization of local cache got expanded. Figure \ref{fig:adal} illustrates
the alignment with additional database sequences being aligned against the same
query sequence simultaneously.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/more_db.png}}
\caption[Additional database sequences]{Additional alignments of database sequences per
iteration\label{fig:adal}}
\end{figure}

\begin{figure}[!h]
\begin{framed}
\footnotesize{
\begin{adjustwidth}{0.8 cm}{-1 cm}
\begin{verbatim}
for (t = 0; t < SW_MULTIPLE; t++) {
    tmp[t] = score_matrix(db, query);
}
SM = *(__m512i*)tmp;
ALIGN(H0, N2, E, F0, SM, S);

for (t = 0; t < SW_MULTIPLE; t++) {
    tmp[t] = score_matrix(db+x , query);
}
SM = *(__m512i*)tmp;
ALIGN(H1, N1, E, F1, SM, S);

\end{verbatim}
\end{adjustwidth}
}
\end{framed}
\caption{\label{code2} SIMD code for multiple alignments per iteration.}
\end{figure}


To be able to utilize the same function as earlier, and having a conscious cache
utilization, an additional padding and distribution of 32 and 64 sequences was
necessary.  This was a simple task, the problem was how much padding was added
and did the extra calculations on the padded areas increased the runtime more
than the cache utilization gained? Both a solution for two and four times more
database sequences were implemented. A code example with two times more
sequences are shown in Figure \ref{code2}. The \texttt{ALIGN} function is the
one shown in Figure \ref{code_align}, and the $x$ added to the database in the
second for loop represents getting the next 16 database sequences, thus giving
\texttt{SW\_MULTIPLE} more sequences to align in this iteration.

% TODO help with the explanation
\subsection{Multiple Columns} \label{mul-col}
An optimization used in Swipe \cite{rognes_faster_2011} is to calculate four
columns per iteration, so a natural experiment was to try this out on the Xeon
Phi to see if this would give a speed gain on the coprocessor as well.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/range_figure.png}}
\caption[Multiple columns]{Illustration of calculating a range of columns\label{range_f}}
\end{figure}

Starting from the first row, a range of $x$ columns, i.e. $x$ number of
characters in the database sequences, are calculated for the entire query
sequence. In Figure \ref{range_f} the range is 2 and the coloring shows the
order of calculations. All matrix element of the same color is calculated before
the next color is carried out.

The code for doing this is the same as the one shown in Figure \ref{code2}, with
the exception of what the $x$ represents. Now the x represent getting the next
character in the database sequences already carried out this iteration.

With this approach there is less need to cache in between values, which gives
more room in the local cache for additional calculations.

\section{Pragma Directives}
When programming one can use a pragma to specifie how a compiler should process
its input. In some cases pragmas specify global behavior, while in other cases
they only affect a local section, such as a block of programming code.  Relevant
in this thesis are the pragmas concerning parallelization, vectorization and
memory management.

For distribution of the workload the OpenMP directive is used. As mentioned in
Section \ref{sec:omp}, \texttt{\#pragma omp for schedule(dynamic)}
will distribute the workload in the following loop dynamically at runtime.

To make the loop run in parallel one may add \texttt{parallel} before
\texttt{for} in the already existing pragma. However this will only work if all
work supposed to run in parallel is inside the loop. If for instance a memory
allocation is required for each thread running in parallel it is not optimal
to do this inside the loop, making redundant allocations. A better approaches
is to use \texttt{\#pragma parallel} that will make a parallel
section where the for loop may reside inside. This is exactly how SWIMIC is
implemented.

When it comes to vectorization the compiler auto vectorize wherever it can,
however this may not apply to all suitable sections if the compiler is unsure of
vector alignment. The \texttt{\#pragma vector} indicates that the loop should be
vectorized, if it is legal to do so, ignoring normal heuristic decisions about
profitability. In terms of alignment of data adding \texttt{\#pragma vector
aligned} informs the compiler that all data is aligned to the required 64 byte,
thus no checks for alignment is done. This may cause incorrect data if the
memory is not alignment, but since all memory allocations in SWIMIC are using
\texttt{\_mm\_malloc} this is should never be a problem.

\section{Match Handling}
To hold on to the \textit{N} sequences that has the best match a custom array
list was used. Each array element is as shown in Figure \ref{stor}, and contain
the score of the matching sequence and its database index, in addition to the
pointers to the next and previous elements according to the sorting from highest
to lowest score.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/storage.png}}
\caption[Storage structure]{Storage structure for the best matching sequences\label{stor}}
\end{figure}

To have the memory allocated as an array makes it easy to access each element
and then to combine it with pointers from highest to lowest element to have it
as a sorted list as well provides a convenient advantage.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/list.png}}
\caption[Linked list]{Insertion in a sorted doubly linked list \label{list}}
\end{figure}

The \textit{N} array elements only change content when a new match that is
higher than the lowest saved score is found. First the score is compared to the
score pointed to by low, if it is higher, the content in the array element
pointed to by low is updated to reflect the new sequence. To attain the sorting
the "new" element is compared to each element from the highest and added as
shown in Figure \ref{list}. The pointers are updated as a common doubly linked
list, and the dotted line in the figure is before the new element is added while
the solid lines represents the pointers after the insertion.

\section{Parameters}

To run SWIMIC with default settings the only parameters required is \texttt{-d},
a preprocessed database file in FASTA format and, \texttt{-q}, a query also in
FASTA. Other options are shown in Figure \ref{code:parameters}. The values in
parentheses are the default option.


\begin{figure}[!h]
\begin{framed}
\scriptsize{
\begin{verbatim}
Usage: ./sw_s [OPTIONS]
  -h, --help               show help
  -d, --db=FILE            sequence database filename (required)
  -q, --query=FILE         query sequence filename (required)
  -s, --matrix=NAME/FILE   score matrix name or filename (BLOSUM62)
  -t, --threads            number of threads (120)
  -r, --range              number of columns calculated per iteration (10)
  -a, --affinity           the affinity used by OpenMP (balanced)
  -m, --matches            number stored matches (20)
\end{verbatim}
}
\end{framed}
\caption{\label{code:parameters} SWIMIC's parameters.}
\end{figure}


\section{Output}
The output from SWIMIC is divided into two parts. The first is a header with
information about the database, query and alignment specifications.

The second part contains the alignment in a human readable format. Each match
starts with the calculated alignment score and the description. Due to limited
memory on the Xeon Phi, only 30 characters are saved, however the unique
sequence specifier is included in the 30 characters. Following is a
representation of the alignment in three lines.  The first is the database
sequence, the second showcases a symboled mapping of the alignment, and the
third line is the query.  A "\texttt{-}" in the database sequence or query
indicates an insertions or deletion. From the symbol line insertions are
indicated by a "\texttt{+}" and deletions are represented by "\texttt{-}". If it
is a pairwise perfect match between the database sequence and the query, the
matching character is written in the symboled line as well.

An example output is shown in Figure \ref{fig:output}. The query sequence and
the alignment representation is shortened down to to a minimum to showcase a
representative preview. Match 2 - 8 is also discarded in the example for a
simpler, yet cleaner overview.

\begin{figure}[!h]
\begin{framed}
\scriptsize{
%\begin{adjustwidth}{0.8 cm}{-1 cm}
\begin{verbatim}
Database size    : 171625029 residues in 459313 sequences
Query file name  : P20930
Query lengt      : 4061
Score matrix     : blosum62
Gap penalty      : 11 + 1
Max matches      : 10
Threads          : 240
Elapsed          : 16.207916 s
GCUPS            : 43.001780


Query sequence   :
MSTLLENIFAIINLFKQYSKKDKNTDTLSKK ...

1: Sequence aligned with score 21359
Description: sp|P20930.3|FILA_HUMAN RecName ...

DB:  MSTLLENIFAIINLFKQYSKKDKNTDTLSKKELKELLEKEFRQILKNPDDPDMVDVFMDHLDIDH
SYM: MSTLLENIFAIINLFKQYSKKDKNTDTLSKKELKELLEKEFRQILKNPDDPDMVDVFMDHLDIDH
Q:   MSTLLENIFAIINLFKQYSKKDKNTDTLSKKELKELLEKEFRQILKNPDDPDMVDVFMDHLDIDH

...

9: Sequence aligned with score 685
Description: sp|Q9NZW4.2|DSPP_HUMAN RecName ...

DB:  KES-GVLVHEGDR-GRQENTQDG-HKGEGNGSKWAEV--GG-KSFSTYSTLANEEGNIEGWNGDT
SYM:   S+G  -H-G  +- Q -    +H G G G     V++ G+   S      NE-G  E--  DT
Q:   RSSAGER-H-GSHH-QQS-ADSSRHSGIGHGQASSAVRDSGHRGYSGSQASDNE-GHSE--DSDT
\end{verbatim}
%\end{adjustwidth}
}
\end{framed}
\caption{\label{fig:output} A simplified overview of SWIMIC's output.}
\end{figure}

