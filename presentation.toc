\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{\scshape Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{\scshape Background}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Sequence Alignment}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{The Intel Xeon Phi Coprocessor}{10}{0}{2}
\beamer@sectionintoc {3}{\scshape Implementation}{25}{0}{3}
\beamer@subsectionintoc {3}{1}{SWIMIC}{26}{0}{3}
\beamer@subsectionintoc {3}{2}{The Smith-Waterman algorithm}{31}{0}{3}
\beamer@subsectionintoc {3}{3}{Optimizations}{36}{0}{3}
\beamer@sectionintoc {4}{\scshape Results}{40}{0}{4}
\beamer@subsectionintoc {4}{1}{Implementation Testing}{50}{0}{4}
\beamer@subsectionintoc {4}{2}{Performance Analysis}{53}{0}{4}
\beamer@subsectionintoc {4}{3}{Competitiveness}{57}{0}{4}
\beamer@sectionintoc {5}{\scshape Conclusion}{58}{0}{5}
