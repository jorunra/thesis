% \input{introduction.tex}
% \input{background.tex}
% \input{xeonPhi.tex}
% \input{preparation.tex}
% \input{methods.tex}
% \input{result.tex}
% \input{future.tex}
% \input{implementation.tex}
% \input{conclution.tex}


This chapter presents and discusses the results developed and the experiences
learned during the implementation process. Some key aspects are implementation
testing, comparison to other tools and exploitation of the coprocessor.

\input{result-implementation.tex}

\section{Performance Analysis}

Unfortunately VTune, the profiler from Intel that was going to help the analysis
process, failed to install correctly. After numerous attempts and
troubleshooting the profiler had to be abandoned. This meant that much of the
information automatically given from the profiler now had to be examined
manually. This included finding bottlenecks in both memory usage and CPU
performance. In addition appropriate solution to these bottlenecks had to be
manually examined.

\subsubsection{vec-report from the Compiler} \label{sec:vecrep}
The Intel compiler includes an option to give a vectorization report using the
flag \texttt{-vec-report[1-6]} With this option a file with \textit{.optrpt}
extension is created for each .c file. In this report each loop in the code is
evaluated and remarks are written. A code snippet from the report is presented
in Figure \ref{code:vec-unaligned}. The loop evaluated in this example is one of
the for loops in the code form Figure \ref{code2} on page \pageref{code2}.

\begin{figure}[H]
\begin{framed}
\footnotesize{
%\begin{adjustwidth}{-0.8 cm}{-1 cm}
\begin{verbatim}
LOOP BEGIN at SIMD.c(604,13)
   remark #15388: vectorization support: reference tmp has
                  aligned access   [ SIMD.c(605,17) ]
   remark #15389: vectorization support: reference db has
                  unaligned access   [ SIMD.c(605,17) ]
   remark #15381: vectorization support: unaligned access used
                  inside loop body
   remark #15427: loop was completely unrolled
   remark #15415: vectorization support: gather was generated
                  for the variable db:  strided by 1
                  [ SIMD.c(605,58) ]
   remark #15415: vectorization support: gather was generated
                  for the variable score_matrix:
                  indirect access [ SIMD.c(605,31) ]
   remark #15300: LOOP WAS VECTORIZED
   remark #15450: unmasked unaligned unit stride loads: 1 
   remark #15458: masked indexed (or gather) loads: 1 
   remark #15475: --- begin vector loop cost summary ---
   remark #15476: scalar loop cost: 20 
   remark #15477: vector loop cost: 2.180 
   remark #15478: estimated potential speedup: 8.420 
   remark #15479: lightweight vector operations: 8 
   remark #15487: type converts: 2 
   remark #15488: --- end vector loop cost summary ---
LOOP END
\end{verbatim}
%\end{adjustwidth}
}
\end{framed}
\caption[vec-report unaligned]{Part of vec-report without \texttt{\#pragma
vector aligned}\label{code:vec-unaligned}}
\end{figure}

Figure \ref{code:vec-unaligned} shows a report from the code before adding the
\texttt{\#pragma vector aligned}. \texttt{Remark \#15389} tells us that the
memory pointed to by \texttt{db} might be unaligned. Even though \texttt{db} is
allocated using \texttt{\_mm\_malloc} which is a \textit{memory aligned}
\texttt{malloc}, the pointer given to the function does not carry this
information with it. The programmer have to help tell the compiler this by
explicitly using \texttt{pragma}'s. By adding \texttt{\#pragma vector
aligned} before the for loop the compiler knows the data is aligned and
unnecessary checks are skipped.

\begin{figure}[H]
\begin{framed}
\footnotesize{
%\begin{adjustwidth}{-0.8 cm}{-1 cm}
\begin{verbatim}
LOOP BEGIN at SIMD.c(604,11)
   remark #15388: vectorization support: reference tmp has
                  aligned access   [ SIMD.c(605,15) ]
   remark #15388: vectorization support: reference db has
                  aligned access   [ SIMD.c(605,15) ]
   remark #15427: loop was completely unrolled
   remark #15415: vectorization support: gather was generated
                  for the variable score_matrix:
                  indirect access    [ SIMD.c(605,29) ]
   remark #15300: LOOP WAS VECTORIZED
   remark #15458: masked indexed (or gather) loads: 1 
   remark #15475: --- begin vector loop cost summary ---
   remark #15476: scalar loop cost: 20 
   remark #15477: vector loop cost: 1.870 
   remark #15478: estimated potential speedup: 10.320 
   remark #15479: lightweight vector operations: 8 
   remark #15487: type converts: 2 
   remark #15488: --- end vector loop cost summary ---
LOOP END
\end{verbatim}
%\end{adjustwidth}
}
\end{framed}
\caption[vec-report aligned]{Part of vec-report with \texttt{\#pragma vector
aligned}\label{code:vec-aligned}}
\end{figure}


The report presented in Figure \ref{code:vec-aligned} shows the report after
adding the \texttt{pragma}. Now the remarks for the pointer \texttt{db} says
that it has aligned access. With this addition the runtime significantly
improves due to no unnecessary alignment checks, see Figure \ref{plot:pragma}.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/pragmas.png}}
\caption{Auto vectorization using \texttt{\#pragma}\label{plot:pragma}}
\end{figure}

\subsection{Auto Vectorization}
The Xeon Phi possesses a variety of build-in features for auto vectorization.
They are automatically enabled with the -O2 or higher compiler flag. A quick
test with vectorization disabled shows that with a query of length 1115 and
a range of 10 the GCUPS calculated is only 0.669! On average the application
calculates 43.4 GCUPS for this query length when vectorization is enabled.

In some cases the compiler flag for optimization is enough to exploit the
resources available, but especially on the Xeon Phi the programmer has to help
the compiler by stating where vectorization may be better utilized. To find this
sections the vec-report compiler flag may be helpful.

\subsubsection{Optimization with Compiler Flags}
In some cases using the -O3 optimization flag may over optimize the code and
yield a slower performance than with -O2. To test if this is the case for SWIMIC
Figure \ref{plot:flag} shows an execution with both.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/flag.png}}
\caption{Optimization flag}{\label{plot:flag}}
\end{figure}

As you can see, there is hardly any difference.

\subsection{Memory Usage} \label{sec:ana}
To examine memory usage a few analysis of performance with different amount of
calculations were carried out. The same tests also determine wherever the
application is limited by either memory or compute power.

The first test examines the performance with twice the amount of computations as
the regular algorithm. This helps determine if there are limitations in memory
usage. If by doubling the calculations the performance is halved the application
is only limited by computations and memory is no obstacle. On the other hand, if
the performance shows no or small affection, the application is not limited by
computation, but is bound by memory. From figure \ref{plot:2x}, that shows the
performance analysis of twice the calculations one can see that the performance
is influenced by the change in workload but not halved. This indicates that the
application is to some extend bound by both memory and computation, but that
there is still some potential performance gain not exploited yet in both
areas.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/2xCalculation.png}}
\caption{Performance analysis of doubling the calculations}{\label{plot:2x}}
\end{figure}

For validity a counter test with no calculations was performed. In this analysis
all memory is set up as used in the regular implementation, except the call to
the \texttt{ALIGN} define (Figure \ref{code_align} on page \pageref{code_align})
is never called, hence no intrinsics are performed. The plot in Figure
\ref{plot:noC} showed a surprising result. How come the performance breaks down
at a query length at approximately 800 amino acids? This behavior was not
expected. However the analysis is highly valuable as it highlights a memory
limitation. The only memory used in the Smith-Waterman algorithm
\cite{smith_identification_1981} that varies with the length of the query is the
\texttt{HE\_array} used to store the previous column of the H and E array.
This reveals that with long query sequences the saving of H and E values causes
cache exceedance and performance loss due to cache read misses.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/noCalculation.png}}
\caption{Performance analysis with no calculations performed}{\label{plot:noC}}
\end{figure}

To verify this assumption a new memory analysis where executed. This time the
saving of H and E values where done to the same fixed location in memory to
examine the performance without a variable depending on the query length. The
result is shown in Figure \ref{plot:noHE}. As expected the performance continues
to increase past a query length of 800 amino acids, thus the \texttt{HE\_array}
is a memory limiting factor.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/noHEarray.png}}
\caption{Performance analysis with no calculations performed and saving the result
at the same location in memory}{\label{plot:noHE}}
\end{figure}

From a theoretical analysis calculating the cache usage the same result appear.
The \texttt{HE\_array} is 2 times the query length times the size of
\texttt{int} for each database sequence.
\begin{align}\label{eq:he}
    HE\_array = 2 * q\_len * sizeof(int).
\end{align}

Since 16 sequences are aligned simultaneously in one vector the
\texttt{HE\_array} is multiplied by 16.  There are T number of thread executing
the calculations concurrently giving the total memory usage for the
\texttt{HE\_array}

\begin{align}\label{eq:me}
    MEM_{HE\_array} &= HE\_array * 16 * T.
\end{align}

If the query length is 900 amino acids and the number of threads is 240 this gives 

\begin{align}
    MEM_{HE\_array} &= HE\_array * 16 * T \nonumber \\
                    &= 27648000 \nonumber \\
                    &\approx 27 MB.
\end{align}


The local vectors used to store in between values such as F and SM is also needed
in cache as well as the additional vector to save the calculated score.
This is close to the 31 MB available cache total on the Xeon Phi coprocessor,
and the result spotted in Figure \ref{plot:noC} is proved. \\

To enhance the application to prevent this limitation from being a performance
drawback a different approach on calculation order is necessary. Some
alternative approaches including a box calculation is discussed in the future
work chapter.


\subsubsection{Utilization of Compute Power}

As Figure \ref{plot:2x} revealed, the application is limited to some extend by
computation. This may be a result of order dependent calculations in the main
loop in the Smith-Waterman algorithm sketched in Figure \ref{code_align}. The
Xeon Phi are able to start executing a new instruction in the next clock
cycle if the following instruction does not depend on the previous one. Figure
\ref{fig:pip} shows the optimal process of execution where the new execution
starts in the next cycle.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.6]{figures/pipeline.png}}
\caption{Optimal use of pipeline}{\label{fig:pip}}
\end{figure}

Unfortunately in some cases the next calculation depend on the result of previous
calculations and the processing unit is waiting to continue and
computer power is gone to waist. In the worst case Figure \ref{fig:bpip} shows
that the pipeline have to wait until the data from the first calculation is
written back to memory before the execution is able to start.
For SWIMIC this may be some of the reason for the compute limitation.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.4]{figures/badPipeline.png}}
\caption{Dependant operations resulting in inefficient pipelining}{\label{fig:bpip}}
\end{figure}


\section{Validity}
To ensure the validity of the application the important aspects to look into is
the calculated score of similarity. Due to the accuracy in the Smith-Waterman
algorithm testing the validity of the calculated score is neglected. If the
calculated total score matches the score calculated from for instance SWIPE
\cite{rognes_faster_2011}, the algorithm is implemented correctly and further
validation testing is not a necessity.


\section{Comparison to Other Tools}
Unfortunately due to limited time and resources a full examination against the
competing tools was  not possible. A good substitute is the comparison work done
by Liu and Smith \cite{liu_swaphi:_2014}, comparing SWAPHI against SWIPE
\cite{rognes_faster_2011} and the BLAST+ \cite{camacho_blast+:_2009}. These are
not the initially intended tools to compare SWIMIC against, however they
includes the two most interesting tools: SWIPE and SWIPHI. \\

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{figures/swaphi-comparison.png}}
\caption{The comparison of tools done for SWAPHI}{\label{plot:comp}}
\end{figure}

The article \cite{liu_swaphi:_2014} states:
\textit{" By using InterSP, SWAPHI achieves a performance of up to 58.8 GCUPS on
    a single Xeon Phi and up to 228.4 GCUPS on four Xeon Phis.  \\\\
    Subsequently, we have compared SWAPHI to SWIPE (v2.0.7) and BLAST+ (v2.2.28)
    (see Fig \ref{plot:comp} (b)).  SWAPHI used the InterSP variant and each
    algorithm used its default scoring scheme. Additionally, SWIPE and BLAST+
    used other options ”-b 0 -v 0” and ”-num alignments 0”, respectively. On
    four Xeon Phis, SWAPHI could not outper- form BLAST+ on 16 CPU cores, but is
    superior to SWIPE on 16 cores. Compared to BLAST+ on 8 cores, SWAPHI
    performs better for most queries and runs 1.19 × faster on average (1.86 ×
    maximally).  Compared to SWIPE on 8 and 16 cores, SWAPHI gives a speedup of
    2.49 and 1.34 on average (2.83 and 1.52 maximally), respectively. "} \\



Comparing SWIMIC to these results is a little disappointing. Despite a lot of
effort, the performance accomplished does not measure up to the high level of
performance accomplished by other tools. However SWIMIC performance compared to
the performance of using a single Xeon Phi with SWAPHI is not as bad as it may
look.  SWIMIC's 43 GCUPS compared to the 58 GCUPS accomplished with SWAPHI is
74 \%.

\begin{table}[!h]
\centering
\begin{tabular}{l r}
\hline
Tool & GCUPS achieved \\ \hline\hline
CUDASW++ 3.0& 119 \\
SWIPE (dual 6 cores CPU) & 106 \\
SWAPHI (One Xeon Phi) & 58 \\
SWIMIC (One Xeon Phi) & 43 \\ \hline
\end{tabular}
\caption{Leading tools and their achieved GCUPS\label{tab:tools}}
\end{table}

Table \ref{tab:tools} presents the GCUPS achieved for the tools SWIMIC is
compared to. The results are taken from each tools' own article.

% Looking at the
% hardware used, the result from SWIPE is on a dual Intel Xeon X5650 six-core
% processor, which is a high standard CPU not commonly present in regular
% computers. To get that standard with Xeon Phi, perhaps two coprocessor cards are
% comparable.
