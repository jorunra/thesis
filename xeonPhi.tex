% \input{introduction.tex}
% \input{background.tex}
% \input{xeonPhi.tex}
% \input{tools.tex}
% \input{implementation.tex}
\section{The Intel Xeon Phi Coprocessor}

The hardware utilized in this thesis is the Intel Xeon Phi coprocessor. It has
attracted attention in the super computing world and several of the most
powerful supercomputers utilizes the Xeon Phi as their computation unit
\footnote{http://www.top500.org/lists/2014/11/}.

\subsection{Technical specifications}

A few key technical specifications about the Xeon Phi, from the Best Practice
Guide Intel Xeon Phi v1.1 by Barth et al.  \cite{xeonguide2013}, supplemented by
Jeffers' and Reinders' \cite{jeffers_intel_2013} book, is described below.

\subsubsection*{General}
The Intel Xeon Phi coprocessor can be looked at from a programmers point of view
as an x86-based SMP-on-a-chip with roughly 60 cores, with multiple hardware
threads per core, and 512-bit SIMD instructions.  All 60 cores has the same
fundamentals as the original Pentium design and are in-order dual issued x86
processor cores which means it can sustain executing two instructions per cycle.
In addition from the Pentium design it also offers a 64-bit support, four
hardware threads per core, power management, ring interconnect support and
512-bit SIMD capabilities. An overview is shown in Figure \ref{core}. Each core
is connected in a symmetric multiprocessing (SMP) fashion, which involves an
architecture where two or more identical processors are connected to a single,
shared main memory, have full access to all I/O devices, and are controlled by a
single operating system instance that treats all processors equally, reserving
none for special purposes. To connect them all a high performance on-die
bidirectional ring interconnect is used, the Core Ring Interface (CRI).

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.7]{figures/core.png}}
\caption[A Xeon Phi Core]{Architecture of a single Xeon Phi core drawn from the
figures in the Jeffers' and Reinders' book\cite{jeffers_intel_2013} page 8.\label{core}}
\end{figure}

The Xeon Phi coprocessor needs to be connected to an Intel Xeon processor-based
host platform, which is done through a high-speed, point-to-point communication
channel, a PCI Express bus. This gives the opportunity to either run a program
from the host or natively on the coprocessor.

The coprocessor runs a full service Linux operating system designed for a Many
Integrated Core (MIC) architecture and it is supported by standard Intel
development tools including Intel Parallel Studio XE, C/C++ compilers and OpenMP
that all may be highly useful when crating an optimized tool for alignment
search in protein databases.

\subsubsection*{Vector Unit} % XXX
The most interesting new feature the Xeon Phi posses is the new 512-bit wide
Vector Processing Unit (VPU) that looks promising with a possible use of SIMD
instructions to vectorize the Smith-Waterman algorithm. Previous Intel SIMD
extensions are not supported, but a new instructions set including
gather/scatter, fused multiply-add, masked vector instruction etc. are
supported. With the SIMD width of 64-Byte (512-Bit) all data needs to be aligned
to 64-Byte to achieve a good performance.  Most vector instructions on the Xeon
Phi has a 4-clock latency with a 1 clock throughput.

The coprocessor also has a lot of built-in auto-vectorization features. Pragmas
like \textit{\#pragma vector aligned} or \textit{\#pragma simd} may be used to
accomplish this. Auto-vectorization is enabled at default optimization level
\textit{-O2}. Each core's VPU also includes the Extended Math Unit (EMU) that
makes it possible to executes 16 32-Bit integer operations or 8 double-precision
floating point operations per cycle.

\subsubsection*{Cache}
The Xeon Phis cache hierarchy consist of the L1 cache that each core utilizes
solely, and a shared L2 cache and tag directory for all the cores.  The L1
cache consist of a 32 KB L1 instruction cache and 32 KB L1 data cache. It has a
load-to-use latency of 1 cycle, which means that an integer value loaded from
the L1 cache can be used in the next clock cycle by an integer instruction
(vector instructions have different latencies than integer instructions). The
L2 cache contributes 512 KB to the global shared L2 cache storage, inclusive of
the L1 data and instruction cache. The effective total L2 size of the chip is
only 512 KB if every core shares exactly the same code and data in perfect
synchronization, if no cores share any data or code the effective total L2 size
of the chip is up to 31 MB. The actual size of the workload-perceived L2
storage is a function of the degree of code and data sharing among cores and
threads. The raw latency for the L2 cache is 11 clock cycles.

\subsubsection*{Memory}
When it comes to memory access, the Xeon Phi has a 8 GB capacity and a memory
channel interface speed of 5.5 gigatransfers per second (GT/s) on a 60 cores
coprocessor. There are 8 memory controllers each accessing two memory channels.
Each memory transaction is 4 byte of data, resulting in $5.5$ GT/s times $4$
bytes or $22$ GB/s per each $16$ channels, giving a maximum transfer rate of
$352$ GB/s. An effective peak of 50 to 60 percent is realistic to
expect.  The main memory is interleaved across the cores and accessed through
the ring interface as well, with hook memory controllers on the die. By linking
memory ports onto the ring, the interleaving around the cores and ring smooths
out the operation of the coprocessor when all the cores are working.

\subsubsection*{Architecture}
% TODO find some more to add to this section. Maybe add more figures.
\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.9]{figures/mic.png}}
\caption[Architecture overview]{Simplified overview of the Xeon Phi architecture
drawn from the figures in the Jeffers' and Reinders' book\cite{jeffers_intel_2013}
page 9.\label{mic}}
\end{figure}

The sketch in Figure \ref{mic} shows a simplified Xeon Phi architecture with
only 6 cores. The connection via the on-die interconnect ring interface (CRI)
contains the shared L2 cache and Tag Directory (TD) together with memory ports.
The cores, illustrated in Figure \ref{core}, are complex and some of the
components included are the four hardware threads, the vector processing unit and the L1
cache.

\subsection{Compilation} \label{sec:ex-mod}
% TODO Explain in more detail!
When implementing an application for the Xeon Phi coprocessor there are two
models available, either a \textit{native execution model} or an \textit{offloading
model}. A native execution model is where the application is meant to run
natively on the coprocessor alone, where as with the offload model the program
may be viewed as running on processor(s) and selected work is offloaded to the
coprocessor(s). Some aspects with both solutions are discussed below and in
Figure \ref{nvso}. \\

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.9]{figures/nativeVSoffload.png}}
\caption{Offload vs native programming models \label{nvso}}
\end{figure}


The native model may be appropriate if the application contains very little
serial processing and has a modest memory footprint. Since it is slower to do
I/O work on the Xeon Phi, the application should not do extensive I/O work and
Graphical user interface (GUI). The great advantage with the native model is
that there is no need to transfer data between the CPU and the coprocessor,
hence no transfer latency. It is also good for applications that are largely
doing operations that map to parallelism either in threads or vectors. On the
other hand, the native execution model is more constraining and the memory
available is very limited.

If the application is more extensive and utilizes more resources an offload
model is definitely the best choice. Unfortunately offload has some additional
concerns when it comes to allocation, copies and deallocation of data and that
it requires two levels of memory blocking: one to fit the input data onto the
coprocessor, and another within the offload code to fit within the processor
caches and not oversaturate the processor memory subsystem when all cores are
busy.

For communication either OpenMP (Open Multi-Processing) or MPI (Message
Passing Interface) may be used to transfer data between the host and the
coprocessor.

The MPI communication protocol is used to program parallel applications and the
standard includes point-to-point message-passing, collective communications,
group and communicator concepts, process topologies, environmental management,
process creation and management, one-sided communications, extended collective
operations, external interfaces, I/O, some miscellaneous topics, and a profiling
interface \footnote{http://www.mpi-forum.org/docs/mpi-3.0/mpi30-report.pdf}. MPI
is widely used on distributed memory system and computers such as computer clusters.

OpenMP on the other hand is an API that supports multi-platform shared memory
multiprocessing programming on most processor architectures and operating
systems. It is a specification for a set of compiler directives,
library routines, and environment variables that can be used to specify
high-level parallelism in Fortran and C/C++ programs
\footnote{http://openmp.org/openmp-faq.html\#OMPAPI.General}.

\subsection{Gaining Optimal Performance} \label{sec:gop}

When programming for the Intel Xeon Phi coprocessor there are some important
aspects to consider concerning the architecture including optimization
techniques, scaling, alignment of data and memory usage.

A single Xeon Phi core is slower than a Xeon core due to lower clock frequency,
smaller caches and lack of sophisticated features such as out-of-order execution
and branch prediction. To fully exploit the processing power of a Xeon Phi
parallelism on both instruction level (SIMD) and thread level (OpenMP) is
needed. Xeon Phi can only perform memory reads/writes on 64-byte aligned data
therefore any unaligned data will be fetched and stored by performing a masked
unpack or pack operation on the first and last unaligned bytes. This may cause
performance degradation, especially if the data to be operated on is small in
size and mostly unaligned.

Because of the relatively small cache on the Xeon Phi it is important to be
aware of data order in memory. Close by data is read into cache for later use,
hence the importance of conscious memory usage. A good question when optimizing
is: \textit{"Is the data in cache the next data to be used for calculations
or is it something completely different?"}. If the cached data is rarely used
unnecessary memory reads drastically slows down the execution time.

Since the vector unit is capable of performing 16 single precision floats or 8
double precision floats per clock cycle, vectorization of an application can
give as much as 8 or 16 times speedup. The VPU also posses the ability of
\textit{Fused Multiply-Add} or \textit{Fused Multiply-Subtract} operations which
effectively double the theoretical floating point performance. To reach the
potential speedup gained by utilizing vectorization is not realistic, but a
significantly performance gain is definitely to be expected.

To utilize the four hardware threads the Xeon Phi possesses, the problem needs
to scale well with hundreds of threads. In theory each device has more than 200
threads available and they are used to hide latency implicit in the in-order
micro-architecture. In practice, use of at least two threads per core is
nearly always beneficial.

\subsection*{Problem criteria}
        \begin{adjustwidth}{0.8 cm}{-1 cm}
\begin{description}
\item [Scaling] Are the workload divided in a fashion that it may be
        distributed to at least 200 threads?
\item [Vectorization] Are the application making strong use of vectorization?
\item [Memory usage] Is the memory well addressed for a shared architecture and
        are all data 64-byte aligned?
\item [Cache usage] Is the next data in memory the next needed data?\\
\end{description}
\end{adjustwidth}

With all these criteria considered a performance boost from the Xeon Phi should
be gained. However, if the outcome still do not meet the anticipated performance
there might be some fine tuning that may improve the run time. By either taking
control with intrinsics or give control in terms of extensive use
of \texttt{pragma} directives, an additional performance boost may appear. Despite an
unique architecture designed for parallelization, there are unfortunately not
all algorithms/problems that are suitable to be executed on the Xeon Phi and
other hardware might yield a better result.
