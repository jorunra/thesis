% \input{introduction.tex}
% \input{background.tex}
% \input{xeonPhi.tex}
% \input{preparation.tex}
% \input{methods.tex}
% \input{result.tex}
% \input{future.tex}
% \input{implementation.tex}

This chapter presents the techniques needed to create an optimized alignment
tool. The two most common algorithms for sequence alignment is discussed
as well as the tools that utilize them. In addition a general idea of
optimization techniques are presented.


\section{Common Approaches for Alignment} \label{com_a} % XXX Better title?

A look into the two most common methods of doing a sequence alignment search,
BLAST \cite{altschul_basic_1990} and Smith-Waterman
\cite{smith_identification_1981}, is described below with their reliability and
speed as key aspects.

\subsection{BLAST}

BLAST uses a technique designed for solving a problem more quickly when classic
methods are too slow, or for finding an approximate solution when classic
methods fail to find any exact solution, called heuristic.  This is achieved by
trading optimality, completeness, accuracy, or precision for speed. In a way, it
can be considered a shortcut.  It does not guarantee the optimal alignment, but
with a heuristic that approximates the Smith-Waterman algorithm the result is
more that acceptable.  It does not compare either sequences in its entirety, but
rater locates short matches between the two. The main idea of BLAST is that
statistically significant alignments often contains segment pairs that do not
increase its score while either extending or shortening down its length, also
known as high-scoring segment pairs (HSP).

The first step of BLAST is to find matching segment pairs, word pairs, with
length \textit{w}, that score at least \textit{T}. The words to compare with
the database sequences are found by matching all possible \textit{w} letter
words out of the 20 amino acids to the query sequence, and save the words that
have a score higher than \textit{T} for at least one word in the query
sequence. For protein sequences the word length, \textit{w}, is usually 3. The
database is then searched for occurrences of the saved words from the query
sequence to find hits.  The hits is extended to high-scoring segment pairs to
check if they score higher than a threshold \textit{V}. This threshold is
determined such that there is reason to believe homology
\cite{eidhammer_protein_2004}.

Depending on \textit{T} and \textit{w}, the sensitivity is determined. While
increasing \textit{T} the runtime of the search decreases, since fewer
word pairs is found and extended. This will also decrease sensitivity
as word pairs might be overlooked. The last step is to use dynamic programming
to align the HSPs that score more than the given threshold to introduce gaps.

The original BLAST only generates ungapped alignments individually, including
the initially found HSPs, even when more than one HSP is found in a database
sequence. Later versions of BLAST \cite{camacho_blast+:_2009} produces a single
alignment with gaps that can include all of the initially found HSP regions.

\subsection{Smith-Waterman} \label{sw_al}

Dynamic programming can be used to find optimal alignment, both global and
local, with few changes to the algorithm.  Needleman and Wunsch
\cite{needleman_general_1970} were the first to use dynamic programming in
bioinformatics to find optimal global alignment.  Their algorithm provided the
foundation for the first approach for optimal local alignment, done by Smith and
Waterman in 1981, hence the later well known Smith-Waterman
algorithm\cite{smith_identification_1981}. The complexity of the algorithm
where $O(m^2n)$, which where later improved by Gotoh \cite{gotoh_improved_1982}
to run at $O(mn)$ by just testing if a gap is elongated, and thus increase the
penalty if true, instead of testing for all possible gap lengths. The
modification is described in more detail in the next section.

Dynamic programming is often used to solve optimization problems where the
problem may have a number of feasible solutions, and the desired solution is the
\textit{best} one. Dynamic programming is a very powerful algorithmic paradigm
in which a problem is solved by identifying a collection of subproblems and
tackling them one by one. Starting with the smallest first and using the answers
to smaller problems to help figure out the larger ones, until the whole set of
problems are solved.

In the Smith-Waterman algorithm the results is stored in a matrix and the score
found for a cell earlier in the solution process is used in later
calculations.

The Smith-Waterman algorithm is divided into two parts. First identify the
highest possible score using dynamic programming, utilizing both a scoring
matrix \textit{s}, like BLOSUM62, and affine gap penalty \textit{W}. Both
presented in Section \ref{sec:altyp}. If the score for a matrix cell is
negative, the cell score is set to zero.

$$
H(i,0) = 0,\; 0\le i\le m.
$$
$$
H(0,j) = 0,\; 0\le j\le n.
$$
$$
H(i,j) = \max \begin{Bmatrix}
0  \\
H(i-1,j-1) + \ s(a_i,b_j) & \text{Match/Mismatch} \\
\max_{k \ge 1} \{ H(i-k,j) + \ W_k \} & \text{Deletion} \\
\max_{l \ge 1} \{ H(i,j-l) + \ W_l \} & \text{Insertion}
\end{Bmatrix},
$$\\

$\text{where }1\le i\le m \text{ and } 1\le j\le n.$\\

\begin{exmp}
Smith-Waterman example from wikipedia
\footnote{http://en.wikipedia.org/wiki/Smith-Waterman\_algorithm, 2014-04-28} with the two sequences,

\begin{center}
\begin{tabular}{rcccccccc}
    Database sequence: &A&C&A&C&A&C&T&A \\
    Query sequence: &A&G&C&A&C&A&C&A
\end{tabular}
\end{center}

\noindent
calculating with a simplified scoring matrix $s(a, b) = +2$ if $a = b$ (match),
$-1$ if $a \neq b$ (mismatch), gives the resulting matrix

\definecolor{airforceblue}{rgb}{0.36, 0.54, 0.66}
$$
H =
\begin{pmatrix}
  & - & A & C & A & C & A & C & T & A \\
    - & \textbf{\color{airforceblue}0} & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
A & 0 & \textbf{\color{airforceblue}2} & 1 & 2 & 1 & 2 & 1 & 0 & 2 \\
G & 0 & \textbf{\color{airforceblue}1} & 1 & 1 & 1 & 1 & 1 & 0 & 1 \\
C & 0 & 0 & \textbf{\color{airforceblue}3} & 2 & 3 & 2 & 3 & 2 & 1 \\
A & 0 & 2 & 2 & \textbf{\color{airforceblue}5} & 4 & 5 & 4 & 3 & 4 \\
C & 0 & 1 & 4 & 4 & \textbf{\color{airforceblue}7} & 6 & 7 & 6 & 5 \\
A & 0 & 2 & 3 & 6 & 6 & \textbf{\color{airforceblue}9} & 8 & 7 & 8 \\
    C & 0 & 1 & 4 & 5 & 8 & 8 & \textbf{\color{airforceblue}11} & \textbf{\color{airforceblue}10} & 9 \\
A & 0 & 2 & 3 & 6 & 7 & 10 & 10 & 10& \textbf{\color{airforceblue}12}
\end{pmatrix}.
$$\\

\noindent
Then the alignment(s) is identified by going backward from the highest score
following the highest entry upward in the matrix until a 0 is the only next
entry. A matrix with arrows is shown below to better illustrate the alignment.

$$
T =
\begin{pmatrix}
  & - & A & C & A & C & A & C & T & A \\
- & \color{airforceblue}0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
A & 0 & \color{airforceblue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow & \nwarrow \\
G & 0 & \color{airforceblue}\uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \nwarrow & \uparrow \\
C & 0 & \uparrow & \color{airforceblue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \color{airforceblue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow & \nwarrow \\
C & 0 & \uparrow & \nwarrow & \uparrow & \color{airforceblue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \nwarrow & \uparrow & \color{airforceblue}\nwarrow & \leftarrow & \leftarrow & \nwarrow \\
C & 0 & \uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \color{airforceblue}\nwarrow & \color{airforceblue}\leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \color{airforceblue}\nwarrow
\end{pmatrix}.
$$\\

\noindent
This gives the optimal alignment following the arrows.

\begin{center}
\begin{tabular}{rccccccccc}
    Database sequence: &A&-&C&A&C&A&C&T&A \\
    Query sequence: &A&G&C&A&C&A&C&-&A
\end{tabular}
\end{center}

A diagonal arrow reflect a match between the query and the database sequence, a
upward arrow implies a deletion and a left arrow implies an insertion.
\end{exmp}


\subsubsection{Gotoh's Modification of the Smith-Waterman Algorithm}
The modification of Gotoh \cite{gotoh_improved_1982} for affine gap penalty
functions are shown below.


\begin{align*}
H_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i-1,j-1} + SM[q_i,d_j] $\\
            $E_{i,j}$\\
            $F_{i,j}$\\
            $0$\\
        \end{tabular}
    \right\rvert
    \begin{tabular}{c}
        $i > 0$\\
        $\cap$\\
        $j > 0$\\
    \end{tabular}$\\
    $\qquad \qquad \quad  0 \qquad $  $ \quad\qquad  $ $\quad \left\lvert
    \begin{tabular}{c}
        $i = 0$\\
        $\cup$\\
        $j = 0$\\
    \end{tabular} \right .$
\end{tabular}
\right .\\\\
E_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i,j-1} - Q $ \\
            $E_{i,j-1} - R $
        \end{tabular}
    \right\rvert j > 0 $ \\
    $\qquad \quad 0 \qquad $  $ \quad $ $\quad \rvert j = 0$
\end{tabular}
\right . \\\\
F_{i,j} &= \left\{
\begin{tabular}{c}
    max $\left \{
        \begin{tabular}{c}
            $H_{i-1,j} - Q $ \\
            $F_{i-1,j} - R $
        \end{tabular}
        \right\rvert i > 0 $ \\
        $\qquad \quad 0 \qquad $  $ \quad $ $\quad \rvert i = 0$
\end{tabular}
\right .\\\\
S &= \quad \underset{1 \leq i \leq m \cap 1 \leq j \leq n }{\text{max }} \quad H_{i,j}
\end{align*}

The major difference from the original Smith-Waterman algorithm is how the gap
penalty is calculated. By adding $E_{i,j}$ and $F_{i,j}$ matrices, which holds
the score of aligning the same prefixes of the query sequence $q$ and the
database sequence $d$ but ending with a gap in the query and the database,
respectively, the penalty can be updated by only increasing the value of
$E_{i,j}$ or $F_{i,j}$ instead of testing for all possible gap lengths. $Q$
equals a gap opening and one extension, while $R$ is only the gap extension
penalty. $SM$ in the equation is the score from the substitution scoring matrix
of aligning $q_i$ with $d_j$, while $S$ is the overall optimal alignment score
and equals the highest value in $H_{i,j}$.


\section{Existing Tools}

% TODO write more about SWAPHI
The most relevant tool to compare this thesis tool to is SWAPHI \cite{liu_swaphi:_2014}
since it is also implemented on the Xeon Phi coprocessor. It is an application
made to use the offload model where all structural work is done on the Xeon host
processor, while the Smith-Waterman calculations is offloaded to the Xeon Phi
coprocessor. This offers the opportunity to connect multiple Xeon Phi
coprocessor together and distribute the work to more coprocessors.

A Smith-Waterman tool, which is one of the fastest on the marked for CPUs, is
Rognes' tool SWIPE \cite{rognes_faster_2011}. The algorithm is implemented on Intel
processors with SSSE3 with parallelization over multiple database sequences.
Instead of aligning one database sequence against the query sequence at a time,
residues from multiple database sequences are retrieved and processed in
parallel.  Rapid extraction and organization of data from the database sequences
have made this approach feasible. The approach also involves computing four
consecutive cells along the database sequences before proceeding to the next
query residue in order to reduce the number of memory accesses needed.

A second tool that utilizes Smith-Waterman with good results is CUDASW++ 3.0
\cite{liu_cudasw++_2013}. It is coupling CPU and GPU SIMD instructions and
conducting concurrent CPU and GPU computations. To balance the runtime
differences of the CPU and the GPU the distribution of sequences is done
dynamically by their compute power. The optimizations done for the CPU is
employed by the steaming SIMD extension (SSE)-based vector execution units and
multi-threading. For the GPU a SIMD parallelization approach using PTX SIMD
video instruction is used to obtain more data parallelization.

\section{Optimization Techniques}
The concept of optimizing a computer program is to modify the implementation to
make it run more efficiently or use fewer resources. For instance, a computer
program may be optimized so that it runs faster, requires less memory or other
resources, or consumes less power. Which technique that is most suitable depends
on the implemented problem and the hardware accessible. It is important to
consider the space-time tradeoff. This is the tradeoff between calculation in less
time by using more storage space (or memory) and solving a problem using very
little storage space but with longer run time.

\subsection*{Parallelization}
The most common optimizing technique is parallelization, in which more than one
calculation is carried out simultaneously. Either by dividing a large problem
into smaller separate tasks and then solve the non depending parts
simultaneously, or perform the same calculation for various conditions in parallel.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.7]{figures/parallel.png}}
\caption[Singel vs. parallel execution]{A drawing of single vs. parallel execution \label{parallel}}
\end{figure}

Parallelization may be done on several levels, such as data level like
vectorization and SMID, thread level like OpenMP and process level like MPI, all
depending on the problem and the supported parallelism of the hardware.

The common hardware used for parallelism are multi-core and multi-processor
computers having multiple processing elements within a single machine, and
clusters that uses multiple computers to work on the same task.  Specialized
parallel computer architectures have been created to achieve an even higher
level of parallelism, like the Intel Xeon Phi used in this thesis, where a
shared memory is connected between 60 cores that all are created for performing
individual work simultaneously.

The maximum possible speedup of a program computing in parallel is limited by
the time needed for the sequential fraction of the program, also known as
Amdahl's law \cite{amdahl_validity_1967} defined in Definition 4.3.1. From this
one can see that if only 70 \% of the program is possible to execute in parallel
then the maximum speedup is 1/30.

% \begin{adjustwidth}{1.2 cm}{0 cm}

\begin{figure}[!h]
\begin{framed}
\begin{defi} Amdahl's Law \\
Given:

$n \in \mathbb{N} $, the number of threads of execution,

$B\in [0, 1] $, the fraction of the algorithm that is strictly serial,

The time $T \left(n \right)$ an algorithm takes to finish when being executed on
$n$ thread(s) of execution corresponds to:

$$T(n) = T(1) \left(B + \frac{1}{n}\left(1 - B\right)\right)$$

Therefore, the theoretical speedup $S(n)$ of executing a given algorithm on a
system capable of executing $n$ threads of execution is:

$$S(n) = \frac{ T\left(1\right)}{T\left(n\right)} =
\frac{T\left(1\right)}{T\left(1\right)\left(B + \frac{1}{n}\left(1 -
B\right)\right) } = \frac{1}{B + \frac{1}{n}\left(1-B\right)}$$\\
\end{defi}
% \end{adjustwidth}
\end{framed}
\end{figure}

Some algorithms highly depend on previous calculations, thus making it
impossible to do more calculations in parallel. The Smith-Waterman algorithm
discussed in the previous section is one of those, and creative thinking is
required to find a way to parallelize it. Since the alignment search is applied
to a large database, a common way to parallelize the search is to compare one
query sequence against a multiple of database sequences in parallel. The same
goes for the other way round, comparing multiple query sequences against one
database sequence at a time. This combined with vectorization is what made SWIPE
\cite{rognes_faster_2011} the leading tools utilizing the Smith-Waterman algorithm
on a regular CPU.

\subsection*{Vectorization}
Differing from a scalar implementation, in which only one single operation
is performed at once, vectorization is a form of data-parallel programming which
makes the processor perform the same operation simultaneously on \textit{N} data
elements of a vector. A vector may be looked at as a one dimensional array of
scalar objects, like the first row in Figure \ref{simd}.

This method of Single Instruction, Multiple Data (SIMD) is used when the same
operation is performed on a large amount of data. It is particularly applicable to
common tasks like adjusting the contrast in a digital image or adjusting the
volume of digital audio.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.5]{figures/vectorization.png}}
\caption{Single instruction, multiple data (SIMD) \label{simd}}
\end{figure}

In this thesis vectorization will be used to execute the steps in the
Smith-Waterman algorithm on a multiple of database sequences. Even though the
Smith-Waterman algorithm is highly depending of the previous calculations the
steps in each iteration of the algorithm are pretty simple and consist of
approximately 10 sub, max or add vector instructions, which all are possible to
do with Intel's SIMD intrinsics. Figure \ref{simd} shows an example of finding
the maximum value of two integers.
