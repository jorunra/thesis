% \input{introduction.tex}
% \input{background.tex}
% \input{xeonPhi.tex}
% \input{preparation.tex}
% \input{methods.tex}
% \input{result.tex}
% \input{future.tex}
% \input{implementation.tex}

In this chapter the two fundamental aspects of creating a sequence alignment
tool for the Intel Xeon Phi coprocessor are presented. First, how to align
protein sequences and calculate an appropriate similarity score and second, how
the Xeon Phi's unique architecture is structured and best utilized.

% With lack of understanding in either of these fields the finish tool may never
% reach its full potential.

\section{Sequence Alignment}

An important process in searching for related protein sequences is to compare them
using sequence alignment, in which sequences are compared by searching for
common character patterns and establishing residue-residue correspondence among
related sequences. This pairwise sequence alignment is the process of aligning
two sequences and is the basis of database similarity searching.

% TODO mention insertions and deletions
\subsection{Alignment Types} \label{sec:altyp}
To find related sequences either a global or local alignment may be used to
identify regions of similarity within a long sequence. A pairwise perfect match
of amino acids, is to be preferred. However insertion or deletion of entries are
sometimes inevitable. What distinguishes local alignment from global alignment
is which parts of both the query and the database sequence is included in the
result. \\

\noindent
Given the two sequences:
\begin{center}
\begin{tabular}{ccccccccccccc}
    F&T&F&T&A&L&I&L&L&A&V&A&V \\
    F&T&A&L&L&L&A&A&V \\
\end{tabular}
\end{center}

\noindent
The global alignment contains both sequences with the first and last letter
mapped together and the rest aligned to the optimal match.

\begin{center}
\begin{tabular}{ccccccccccccc}
    F&T&F&T&A&L&I&L&L&A&V&A&V \\
    F&-&-&T&A&L&-&L&L&A&-&A&V \\
\end{tabular}
\end{center}

\noindent
For local alignment the subsequences, i.e. the part of the sequences that gives
the best match, is returned. In this case:

\begin{center}
\begin{tabular}{cccccccccc}
    F&T&A&L&I&L&L&-&A&V \\
    F&T&A&L&-&L&L&A&A&V \\
\end{tabular}
\end{center}

\noindent
When comparing protein sequences local alignment is used to find subsequences
in the database which are similar to subsequences in the query. The reason
local alignment is preferred over global is that the query may differ
significantly in length compared to the majority of the database sequences. With
local alignment only regions that are highly similar are taken into account and
unequal parts are discarded.

\subsubsection{Substitution Scoring Matrix}
The alignment procedure has to make use of a scoring system, which is a set of
values for quantifying the likelihood of one residue being substituted by
another in an alignment. These substitution scoring matrices are derived from
statistical analysis and describes the probability rate of which an amino acid
\textit{a} in a sequence is changed to another amino acid \textit{b} in a
certain evolutionary time.

A positive score indicates a more likely frequency of substitution than what
would have occurred in nature by random chance. A score of zero refers to a
substitution equal to what is expected by chance. A negative score means a
frequency of substitution less likely to have occurred by random chance and is
normally the case between dissimilar residues.


%\begin{adjustwidth}{-1 cm}{-1 cm}
\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.5]{figures/BLOSUM62.png}}
\caption[BLOSUM62 matrix]{\label{fig:blos}The BLOSUM62 substitution scoring matrix.}
\end{figure}
%\end{adjustwidth}

The two most widely used series of substitution scoring matrices for amino acids
are PAM and BLOSUM. Figure \ref{fig:blos}
\footnote{http://en.wikipedia.org/wiki/File:BLOSUM62.gif, 2008-03-07} shows
an example, the BLOSUM62 matrix. Both series exemplifies the main aspects
required for a well functioning scoring matrix. The first being the degree of
the "biological relationship" between the amino acids, and the second being the
probability of two amino acids occurring at homologous positions in sequences
that have a common ancestor, or that one is the ancestor of the
other\cite{eidhammer_protein_2004}.

\subsubsection{Gap Penalty}
In order to acquire the best possible match one is often required to either
insert or delete an amino acid entry to align the query sequence with a database
entry. This insertions and deletions requires consideration when calculating the
score of similarity, and is referred to as gap penalty.  The gap penalty is
commonly assigned by an affine function that give an initial penalty for a gap
opening, and an additional penalty which increases in correspondence with the
length of the gap. A typical penalty used as default in SWIPE
\cite{rognes_faster_2011} is \textit{11} for an opening and penalty of
\textit{1} for each extension.

\subsection{Databases}
The sequence databases used to identify new proteins are large and contains
millions of sequences of an average length of 300-400 amino acids. A commonly
used protein sequence database is UniProtKB/Swiss-Prot
\cite{consortium_update_2013}. It is a manually annotated and non-redundant
database. The aim of UniProtKB/Swiss-Prot is to provide all known relevant
information about a particular protein. The manual annotation of an entry
involves detailed analysis of the protein sequence and the scientific
literature. Annotations are regularly reviewed to keep up with current
scientific findings. From the release 2014\_05 of 14-May-14
\footnote{http://web.expasy.org/docs/relnotes/relstat.html} the
UniProtKB/Swiss-Prot database contains 545388 sequence entries, comprising
193948795 amino acids abstracted from 228536 references.


Another possible database for proteins is \emph{GenBank}
\cite{benson_genbank_2013}. In 2013 it contained over 150 billion nucleotide
bases in more than 162 million sequences. It is produced and maintained by the
National Center for Biotechnology Information (NCBI) as part of the
International Nucleotide Sequence Database Collaboration (INSDC). Also built by
NCBI is \textit{The Reference Sequence} (\emph{RefSeq}) database \cite{pruitt_ncbi_2007}.
It differs from \emph{GenBank} in that it only provides a single record for each
natural biological molecule, i.e. DNA, RNA or protein.

\subsection{Input Format}
The databases contains sequences in FASTA format that originates from the
alignment tool FASTP \cite{lipman_rapid_1985}. FASTA is a text based format in
which the amino acids are represented by single letter codes. Each sequence
begins with a single line description followed by the sequence data, see Figure
\ref{fig:fasta} for an example. The description line is distinguished from the
sequence data by an initial '>' symbol. The identifier of the sequence is the
word directly following the '>' and the rest of the line are an optional
description. The end of the sequence is recognized by the start of the next
sequence beginning with '>'.

\begin{figure}[!h]
\newgeometry{textwidth=380pt}
\begin{framed}
\small{
\begin{verbatim}

>MCHU - Calmodulin - Human, rabbit, bovine, rat, and chicken
ADQLTEEQIAEFKEAFSLFDKDGDGTITTKELGTVMRSLGQN
PTEAELQDMINEVDADGNGTIDFPEFLTMMARKMKDTDSEE
EIREAFRVFDKDGNGYISAAELRHVMTNLGEKLTDEEVDEMI
READIDGDGQVNYEEFVQMMTAK
\end{verbatim}
}
\end{framed}
\restoregeometry
\caption[FASTA sequence]{\label{fig:fasta} An example sequence in FASTA format
\footnotemark}
\end{figure}

\footnotetext{http://en.wikipedia.org/wiki/FASTA\_format, 2014-05-09}

% TODO delete?
% \subsection{Methods}
% After the sequence database is read into memory a search is performed, either
% with BLAST\cite{altschul_basic_1990} or
% Smith-Waterman\cite{smith_identification_1981}, to find the local alignment.
% BLAST is time efficient, but does not guarantee to find the optimal match in all
% cases because of the reduced sensibility in the heuristic algorithm.
% Smith-Waterman on the other hand is a highly reliable algorithm, and will always
% give the formally correct sequence having the highest optimal alignment score,
% but it is more time-consuming compared to BLAST. Both BLAST and the
% Smith-Waterman algorithm is described in more detail and with examples in
% chapter \ref{com_a}.

% TODO add figure of both the human readable format
% Use BLAST output option -m 6.
% \subsection{Output format}
% The result from an alignment search is usually returned in a human readable
% format, for instance HTML, XML or plain text. There is no standard output format
% for protein alignment, but it is possible to use the DNA alignment standard,
% Sequence Alignment/Map (SAM) \cite{li_sequence_2009}.
% 
% \subsubsection*{Tab Separated Output} % XXX
% 
% 
% \subsubsection*{SAM}
% SAM is text based and stores data in tab delimited ASCII columns with
% \textit{11} mandatory fields, described in Table \ref{sam}\footnotemark[3].
% That particular order is required and they describes the key information about
% the alignment.  SAM allows an optional header starting with \textit{'@'} to
% describe the file and to add additional information and columns, e.g.
% information about the reference sequence or aligner specific information. SAM is
% flexible enough to store alignment information generated by various alignment
% tools, yet simple enough to easily convert to existing alignment formats. The
% file size is compact, and allows most of the operations on the alignment to work
% on a stream without loading the whole alignment into memory.
% 
%  
%  \begin{table}[h!]
%  \begin{adjustwidth}{-0.5 cm}{-0.5 cm}
%  \begin{center}
%  \footnotesize
%  \begin{tabular}{rllll}
%  \hline
%  {\bf Col} & {\bf Field} & {\bf Type} & {\bf Regexp/Range} & {\bf Brief description} \\
%  \hline
%  1 & {\sf QNAME} & String & {\tt [!-?A-\char126]\{1,255\}} & Query template NAME\\
%  2 & {\sf FLAG} & Int & {\tt [0,2$^{16}$-1]} & bitwise FLAG \\
%  3 & {\sf RNAME} & String & {\tt \char92*|[!-()+-\char60\char62-\char126][!-\char126]*} & Reference sequence NAME\\
%  4 & {\sf POS} & Int & {\tt [0,2$^{31}$-1]} & 1-based leftmost mapping POSition \\
%  5 & {\sf MAPQ} & Int & {\tt [0,2$^8$-1]} & MAPping Quality \\
%  6 & {\sf CIGAR} & String & {\tt \char92*|([0-9]+[MIDNSHPX=])+} & CIGAR string \\
%  7 & {\sf RNEXT} & String & {\tt \char92*|=|[!-()+-\char60\char62-\char126][!-\char126]*} & Ref. name of the mate/next read\\
%  8 & {\sf PNEXT} & Int & {\tt [0,2$^{31}$-1]} & Position of the mate/next read \\
%  9 & {\sf TLEN} & Int & {\tt [-2$^{31}$+1,2$^{31}$-1]} & observed Template LENgth \\
%  10 & {\sf SEQ} & String & {\tt \char92*|[A-Za-z=.]+} & segment SEQuence\\
%  11 & {\sf QUAL} & String & {\tt [!-\char126]+} & ASCII of Phred-scaled base QUALity+33 \\
%  \hline
%  \end{tabular}
%  \end{center}
%  \end{adjustwidth}
%  \caption[SAM format description]{Description of the \textit{11} mandatory columns in SAM format \label{sam}}
%  \end{table}
%  
%  
% The example in Figure \ref{sam2} is from samtools own page \footnotemark[3]
% \footnotetext[3]{https://samtools.github.io/hts-specs/SAMv1.pdf, 2015-03-25} and
% shows an alignment and the corresponding SAM format.
% 
% \begin{figure}[!h]
% \newgeometry{textwidth=420pt}
% \begin{framed}
% \scriptsize{
% \begin{verbatim}
% Coor     12345678901234  5678901234567890123456789012345
% ref      AGCATGTTAGATAA**GATAGCTGTGCTAGTAGGCAGTCAGCGCCAT
% 
% +r001/1        TTAGATAAAGGATA*CTG
% +r002         aaaAGATAA*GGATA
% +r003       gcctaAGCTAA
% +r004                     ATAGCT..............TCAGC
% -r003                            ttagctTAGGC
% -r001/2                                        CAGCGGCAT
% \end{verbatim}
% }
% \end{framed}
% 
% \begin{framed}
% \scriptsize {
% \begin{verbatim}
% @HD VN:1.5 SO:coordinate
% @SQ SN:ref LN:45
% r001   99 ref  7 30 8M2I4M1D3M = 37  39 TTAGATAAAGGATACTG *
% r002    0 ref  9 30 3S6M1P1I4M *  0   0 AAAAGATAAGGATA    *
% r003    0 ref  9 30 5S6M       *  0   0 GCCTAAGCTAA       * SA:Z:ref,29,-,6H5M,17,0;
% r004    0 ref 16 30 6M14N5M    *  0   0 ATAGCTTCAGC       *
% r003 2064 ref 29 17 6H5M       *  0   0 TAGGC             * SA:Z:ref,9,+,5S6M,30,1;
% r001  147 ref 37 30 9M         =  7 -39 CAGCGGCAT         * NM:i:1
% \end{verbatim}
% }
% \end{framed}
% \restoregeometry
% \caption[SAM format]{An example with an alignment and the corresponding SAM
% format \label{sam2}}
% \end{figure}

% Since SAM may be slow to parse and is created to be human readable, the binary
% equivalent format BAM may also be used as output from an alignment search,
% especially if the result is going to be post processed.
